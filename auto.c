#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct my_msgbuf {
    long mtype;
    char mtext[50];
};

int sleep_ns_min = 100, sleep_ns_max = 2000;

int main(int argc, char *argv[]) {
	char *buf_reg, *buf_smjer;
	char *reg_str =argv[1];
	long reg_oznaka = strtol(reg_str, &buf_reg, 10);
	long smjer = strtol(argv[2], &buf_smjer, 10);

	// cekaj X sekundi
	srand(time(0));
	struct timespec ts;
	ts.tv_sec = 0;
	ts.tv_nsec =  (rand() % (sleep_ns_max + 1 - sleep_ns_min) + sleep_ns_min) * 1000000L;

	nanosleep(&ts, NULL);

	// probudi se i upisi zahtjev registracija u prikladni red zahtjeva (za smjer 0/1)
	int msqid;
	
	key_t key = smjer ? 1 : 10;
	if ((msqid = msgget(key, 0600 | IPC_CREAT)) == -1) {
		perror("msgget");
		exit(1);
	}

	struct my_msgbuf buf;
	memcpy(buf.mtext, reg_str, strlen(reg_str)+1);
	buf.mtype = 1;
 	if (msgsnd(msqid, (struct msgbuf *)&buf, strlen(reg_str)+1, 0) == -1)
            perror("msgsnd");
	printf("Automobil %ld ceka na prelazak preko mosta\n", reg_oznaka);


	// cekaj dok se ne pojavi poruka s tip=registracija u redu Predi
	struct my_msgbuf buf_msg_rcvd;
	int msqid_predi;
	if ((msqid_predi = msgget(20, 0600 | IPC_CREAT)) == -1) {
		perror("msgget");
		exit(1);
	}
	if (msgrcv(msqid_predi, (struct msgbuf *)&buf_msg_rcvd, sizeof(buf_msg_rcvd)-sizeof(long), reg_oznaka, 0) == -1) {}

	// dobio poruku Predi - popne se na most
	printf("Automobil %ld se popeo na most\n", reg_oznaka);


	// cekaj dok se ne pojavi poruka s tip=registracija u redu Presao
	int msqid_presao;
	if ((msqid_presao = msgget(30, 0600 | IPC_CREAT)) == -1) {
		perror("msgget");
		exit(1);
	}
	if (msgrcv(msqid_presao, (struct msgbuf *)&buf_msg_rcvd, sizeof(buf_msg_rcvd)-sizeof(long), reg_oznaka, 0) == -1) {}

	// dobio poruku Presao - presao je most
	printf("Automobil %ld je presao most\n", reg_oznaka);

	return 0;

}
