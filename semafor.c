#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <sys/time.h>

struct my_msgbuf {
    long mtype;
    char mtext[50];
};

int trenutni_smjer;
int msqid;
int msqid_odgovor_predi, msqid_odgovor_presao;

int counter_remain;
int sigalrm_triggered = 0;
int num_msgs_last_consumed;

int prelaz_min = 1000, prelaz_max = 3000;


void consume_three_messages() {
	// ako je zavrsio timeout, postaviti ovu global var -> 1 (prekida cekanje na 3 zahtjeva)
	sigalrm_triggered = 1;

	struct msqid_ds msqid_ds;

	if (msgctl(msqid, IPC_STAT, &msqid_ds) == -1) {
		perror("msgctl");
		exit(1);
	}
	int num_msgs_in_queue = msqid_ds.msg_qnum;

	// broj poruka Predji koje ce se poslati = 3 (ili manje ako ima < 3 auta u trenutnom smjeru)
	num_msgs_last_consumed = num_msgs_in_queue >= 3 ? 3 : num_msgs_in_queue;
	
	struct my_msgbuf buf_msg_rcvd;
	struct my_msgbuf buf_msg_pass, buf_msg_passed;

	long reg_oznake[num_msgs_last_consumed];

	// primi <= 3 zahtjeva iz reda poruka u trenutnom smjeru (odreden sa msqid - global var postavljena u main-u)
	for (int i=0; i<num_msgs_last_consumed; i++) {
		if (msgrcv(msqid, (struct msgbuf *)&buf_msg_rcvd, sizeof(buf_msg_rcvd)-sizeof(long), 0, 0) == -1) {
			printf("rcv\n");
			perror("msgrcv");
			exit(1);
		}

		
		char *buf_reg;
		long reg_oznaka = strtol(buf_msg_rcvd.mtext, &buf_reg, 10);

		
		reg_oznake[i]=reg_oznaka;
	 	
	}
	
	// salji poruke Predi
	// VAZNO: tip poruke = reg oznaka auta iz primljenog zahtjeva
	// -> prema tipu ce Auto koji ceka svoj Predi znati procitati poruku za sebe
	char *text_send_predi = "Predi";
	memcpy(buf_msg_pass.mtext, text_send_predi, strlen(text_send_predi)+1);
	for (int i=0; i<num_msgs_last_consumed; i++) {
		buf_msg_pass.mtype = reg_oznake[i];

		if (msgsnd(msqid_odgovor_predi, (struct msgbuf *)&buf_msg_pass, strlen(text_send_predi)+1, 0) == -1)
			    perror("msgsnd");
	}
	

	// cekaj Y sekundi - prijelaz mosta
	struct timespec ts;
	ts.tv_sec = 0;
	ts.tv_nsec =  (rand() % (prelaz_max + 1 - prelaz_min) + prelaz_min) * 1000000L;

	nanosleep(&ts, NULL);

	// salji poruke Presao
	// vazno: tip poruke = reg oznaka auta iz primljenog zahtjeva (analogno kao i za slanje poruka Predi)
	char *text_send_presao = "Presao";
	memcpy(buf_msg_passed.mtext, text_send_presao, strlen(text_send_presao)+1);
	for (int i=0; i<num_msgs_last_consumed; i++) {
		buf_msg_passed.mtype = reg_oznake[i];
		if (msgsnd(msqid_odgovor_presao, (struct msgbuf *)&buf_msg_passed, strlen(text_send_presao)+1, 0) == -1)
			perror("msgsnd");

	}
	
	// azurirati broj preostalih auta koji nisu jos presli
	counter_remain -= num_msgs_last_consumed;
	// obrnuti smjer - u sljedecem krugu ce se propustiti auti iz suprotnog smjera
	trenutni_smjer = !trenutni_smjer;
}



int main(int argc, char *argv[])
{

	srand(time(0));

	int Nmin = 5, Nmax = 100;
	int N = rand() % (Nmax + 1 - Nmin) + Nmin;
	printf("N = <%d>\n", N);

	// generirati random smjer 0/1 za svaki auto
	int smjerovi[N];
	for (int i=0; i<N; i++) {
		smjerovi[i] = rand() % 2;
	}

	char buf_smjer[2];
	char buf_regist[4];	
	// stvori N procesa Auto
	for (int i=1; i<=N; i++) {
		switch (fork()) {
			case -1:
				printf("Ne moze kreirati novi auto\n");
				break;
			case 0:
				sprintf(buf_regist, "%d", i);
				sprintf(buf_smjer, "%d", smjerovi[i-1]);

				// u novom procesu kreira novi Auto(reg oznaka, smjer) - novi program auto
				execl("./auto", "auto", buf_regist, buf_smjer, NULL);
				
				exit(1);
				
		}

		
	}



	struct msqid_ds msqid_ds;


	int msqid0, msqid1;
	int msqid_pass_over;

	// kljucevi redova za primanje zahtjeva 2 smjera
	key_t key_smjer0 = 10, key_smjer1 = 1;

	// red poruka za zahtjeve u smjeru 0
	if ((msqid0 = msgget(key_smjer0, 0600 | IPC_CREAT)) == -1) {
		perror("msgget");
		exit(1);
	}
	// red poruka za zahtjeve u smjeru 1
	if ((msqid1 = msgget(key_smjer1, 0600 | IPC_CREAT)) == -1) {
		perror("msgget");
		exit(1);
	}
	
	// red poruka za poruke Predi
	if ((msqid_odgovor_predi = msgget(20, 0600 | IPC_CREAT)) == -1) {
		perror("msgget");
		exit(1);
	}
	// red poruka za poruke Presao
	if ((msqid_odgovor_presao = msgget(30, 0600 | IPC_CREAT)) == -1) {
		perror("msgget");
		exit(1);
	}



	counter_remain = N;		// broj auta koji jos nisu presli
	trenutni_smjer = rand() % 2;	// pocetni random smjer 0/1
	
	int X_wait_min = 500, X_wait_max = 1000;


	
	while (counter_remain > 0) {
		// uzeti red poruka zahtjeva za trenutni smjer
		msqid = trenutni_smjer ? msqid1 : msqid0;

		// postaviti alarm koji se aktivira nakon X us (ako prije aktivacije nisu vec zabiljezena 3 zahtjeva)
		int X_wait = (rand() % (X_wait_max + 1 - X_wait_min) + X_wait_min) * 1000L; 
		sigset(SIGALRM, consume_three_messages);
		ualarm(X_wait, 0);

		// ocekivanje 3 zahtjeva auta
		// prekida ako istece timeout
		while (1) {
			if (sigalrm_triggered) {
				break;
			}
			if (msgctl(msqid, IPC_STAT, &msqid_ds) == -1) {
				perror("msgctl");
				exit(1);
			}
			if (msqid_ds.msg_qnum == 3) {
				signal(SIGALRM, SIG_DFL);
				consume_three_messages();
				break;
			}
		}

	
		// resetira flag za alarm
		sigalrm_triggered = 0;

	}

	
	printf("Svi auti su presli. Kraj\n");
	// zatvoriti sve redove poruka
	if (msgctl(msqid0, IPC_RMID, NULL) == -1) {
		perror("msgctl");
		exit(1);
	}
	if (msgctl(msqid1, IPC_RMID, NULL) == -1) {
		perror("msgctl");
		exit(1);
	}

	if (msgctl(msqid_odgovor_predi, IPC_RMID, NULL) == -1) {
		perror("msgctl");
		exit(1);
	}
	if (msgctl(msqid_odgovor_presao, IPC_RMID, NULL) == -1) {
		perror("msgctl");
		exit(1);
	}

	

	return 0;
}
