Message Queues Process Communication according to distributed centralized protocol with a semaphore process for mutual exclusion. Implemented in C using <sys/types.h>, <sys/ipc.h> and <sys/msg.h> header files.

My lab assignment in Advanced Operating Systems, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2021
